package ru.tsc.denisturovsky.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.denisturovsky.tm.configuration.ServerConfiguration;

@Getter
public final class ServerContext {

    @NotNull
    public static final ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

}
